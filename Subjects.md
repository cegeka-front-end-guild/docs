## Intro to Web
    - overview 1 slide big technologies used 
	- interracting with web server, http requests, request methods (postman)
    - page request lifecycle
    - performance: CDN, minification, concatenation 
    - GIT useful commands ?

## HTML fundamentals
    - history
    - anatomy of a page
    - text elements in HTML: headings, block and inline elements, text breaking
    - html lists (ul / ol)
    - html links: anchors, jump into document
    - html table: table structure ?

## Learning / Solving problems
    - MDN (mozzila developer network) / w3schools
    - Podcasts / books (Laur)
    - browser console
    - jsfiddle, jsbin, codepen, plunker, stackblitz

## JavaScript fundamentals
    - other notes can be found in "JavaScript From The Ground Up day1.pptx"
    - variables, strings, numbers, operators, expressions, statements
    - functions (named, anonymous functions)
    - objects: create, add props, retrieve props, delete props
    - program flow: if, while, do while, for, break, continue, switch
    - true and false (== si ===)
    - strings and methods on strings
    - comments ?
    - Data structures ( array, map, [])
        Object-oriented JavaScript
        First-class functions
        Scopes and closures
        Types and grammar
        Node.js basics

## Basic Libraries & Tools
    - jquery
    - package manager: nuget / npm / bower / yarn
    - grunt / gulp / webpack ?
    - 1 slide of react / angular / ember pros and cons

## Typescript fundamentals
    - why / what
    - type annotations
    - type inferences
    - any and primitive types
    - GETTER, SETTER
    - object, functions
    - classes: fields, ctor, props, functinos,
    - interfaces
    - extending ?
    - metode de pe array, string etc.

## CSS fundamentals
    - notiuni de baza (padding / etc) + bootstrap / material

## Angular
    - use angular presentations and exercises (module 1, 2, 3)